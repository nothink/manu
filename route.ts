import { Router, RouterContext } from "https://deno.land/x/oak/mod.ts";
import { getIndex } from "./server.tsx";

const router = new Router();
router
  .get("/", getIndex)

export { router };