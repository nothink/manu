import { createHash } from "https://deno.land/std/hash/mod.ts";

const hash = createHash("sha3-224");
const chr = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
const chrlen: number = chr.length;

let cryptString = "";
let cryptLength = 0;
let diff = 56;
while (diff > 0 || Math.random() < 0.5) {
    const randC = chr[Math.floor(Math.random() * chrlen)];
    if (!cryptString.includes(randC)) diff--;
    cryptString += randC;
    cryptLength++;
}

export const cryptSalt = cryptString.substr(0, 32);

export function hashEmail(str: string): string
{
    hash.update(str);
    return hash.toString();
}

export function randomStr(length: number): string
{
    let id = "";
    for (let i = 0; i < length; i++) {
        id += cryptString[Math.floor(Math.random() * cryptLength)];
    }
    return id;
}