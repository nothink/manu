export type ParamCoach = {
    id?: number,
    team?: number,
    search?: string,
}

export type ParamIncident = {
    fixture: number,
    team?: number,
    player?: number,
    type?: "Goal" | "Card" | "Subst" | "Var",
}

export type ParamMatchStats = {
    fixture: number,
    team?: number,
}

export type ParamLeague = {
    id?: number,
    name?: string,
    country?: string,
    code?: string,
    season?: number,
    team?: number,
    type?: "league" | "cup",
    current?: boolean,
    search?: string,
    last?: number,
}

export type ParamLineup = {
    fixture: number,
    team?: number,
    player?: number,
    //type?: string,
}

export type ParamMatch = {
    id?: number,
    live?: string,
    date?: string,
    league?: number,
    season?: number,
    team?: number,
    last?: number,
    next?: number,
    from?: string,
    to?: string,
    round?: string,
    status?: "TBD" | "NS" | "1H" | "HT" | "2H" | "ET" | "P" | "FT" | "AET" | "PEN" | "BT" | "SUSP" | "INT" | "PST" | "CANC" | "ABD" | "AWD" | "WO" | "LIVE",
    //timezone?: string,
}

export type ParamPlayer = {
    id?: number,
    team?: number,
    league?: number,
    season?: number,
    search?: string,
    page?: number,
}

export type ParamSquad = {
    team?: number,
    player?: number,
}

export type ParamStanding = {
    league?: number,
    season: number,
    team?: number,
}

export type ParamTeam = {
    id?: number,
    name?: string,
    league?: number,
    season?: number,
    country?: string,
    search?: string,
}