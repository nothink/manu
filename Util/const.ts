export const API_URL = Deno.env.get("API_URL") as string;
export const FETCH_INIT = { headers: new Headers({ "x-rapidapi-key": Deno.env.get("API_KEY") ?? "" }) };

export const CHAMPIONS_LEAGUE = 2;
export const EUROPA_LEAGUE = 3;
export const PREMIER_LEAGUE = 39;
export const J_LEAGUE = 98;

export const MANU = 33;

export const timeS = {
    w: 604800000,
    d: 86400000,
    h: 3600000,
    m: 60000,
}