import { fetchApiStatu, fetchCoach, fetchIncident, fetchLeague, fetchLineup, fetchMatch, fetchMatchStatistics, fetchPlayer, fetchSquad, fetchStanding, fetchTeam } from "../Controller/Api.ts";
import { API_URL, MANU, PREMIER_LEAGUE, timeS } from "./const.ts";
import { apiRemain, genParam, trySeason } from "./helper.ts";
import { Coach } from "../Model/Coach.ts";
import { League } from "../Model/League.ts";
import { Match, matchStatus } from "../Model/Match.ts";
import { Player } from "../Model/Player.ts";
import { Season } from "../Model/Season.ts";
import { Standing } from "../Model/Standing.ts";
import { Team } from "../Model/Team.ts";
import { Log } from "../Model/Log.ts";
import { Lineup } from "../Model/Lineup.ts";
import { Head } from "../Model/Head.ts";

let liveId: number | null;
let curSYear = trySeason();

export async function initialFetch() {
    await fetchApiStatu();
    if (!(await Log.lastLeague())) {
        if (apiRemain == 0) { setTimeout(initialFetch, timeS.h)}
        for (let i = 0; i <= 1; i++) {
            await fetchLeague({ team: MANU });
            if (await League.count() != 0 && await Season.count() != 0) break;
        }
    }

    const curSeason = await Season.curSeason(PREMIER_LEAGUE) as Season;
    if (!curSeason) return;

    curSYear = curSeason.year;
    if (await Team.count() == 0) {
        if (apiRemain == 0) { setTimeout(initialFetch, timeS.h)}
        await fetchTeam({ league: PREMIER_LEAGUE, season: curSYear });
    }

    if (await Player.count() == 0) {
        await fetchSquad({ team: MANU });
    }

    if (await Standing.count() == 0) {
        await fetchStanding({ league: PREMIER_LEAGUE, season: curSYear });
    }

    if (await Match.count() == 0) {
        await fetchMatch({ team: MANU, season: curSYear });
    }

    continueFetch();
}

async function continueFetch() {
    const now = new Date();

    while (apiRemain == 0) {
        await fetchApiStatu();
        await new Promise(s => setTimeout(s, timeS.h * (1 + Math.random())));
    }

    const curSeason = await Season.curSeason() as Season;
    if (!curSeason) {
        await fetchLeague({ team: MANU });
    } else {
        curSYear = curSeason.year;

        /*if (liveId) return;
        else {
            const today = new Date(now.getFullYear(), now.getMonth(), now.getDate());
            const todayDateStr = today.toDateString();
            const futureMatchs = await Match.where("date", ">=", today).orderBy("date").get() as Match[];
            const todayMatch = futureMatchs.find(m => new Date(m.date).toDateString() == todayDateStr && matchStatus[m.status] >= 0 && matchStatus[m.status] <= 10);
            if (todayMatch) {
                const mDate = new Date(todayMatch.date);
                if ((await Match.where("id", todayMatch.id).heads() as Head[]).length < 2) {
                    const lineupRelease = mDate.setMinutes(mDate.getMinutes() - 30);
                    if (Date.now() > lineupRelease) await fetchLineup({ fixture: todayMatch.id });
                    else setTimeout(fetchLineup, lineupRelease - Date.now(), { fixture: todayMatch.id });
                }
                liveId = setTimeout(liveFetch, mDate.getTime() - Date.now() + timeS.m, todayMatch.id);
                return;
            }
        }*/
    
        const lastLeaguehLog = await Log.where("param", genParam({ team: MANU })).lastLeague() as Log;
        if (Date.now() > lastLeaguehLog.datetime.getTime() + timeS.d) {
            await fetchLeague({ team: MANU });
        }
        
        const lastMatchLog = await Log.where("param", genParam({ team: MANU, season: curSYear })).lastMatch() as Log;
        if (Date.now() > lastMatchLog.datetime.getTime() + timeS.d) {
            await fetchMatch({ team: MANU, season: curSYear });
            
            const preMatchs = await Match.where("date", "<", now).orderBy("date", "desc").get() as Match[];
            if (preMatchs.length > 0) {
                const lastMatched = preMatchs.find(m => matchStatus[m.status] > 10) as Match;
                const league = await Season.where("code", lastMatched.seasonCode).league() as League;
                await fetchStanding({ league: league.id, season: curSYear });
            }
        }
        
        const lastPlayerLog = await Log.where("param", genParam({ team: MANU, season: curSYear })).lastPlayer() as Log;
        if (Date.now() > lastPlayerLog.datetime.getTime() + timeS.d && now.getDay() == 2) {
            await fetchPlayer({ team: MANU, season: curSYear });
        }
        
        const lastSquadLog = await Log.where("param", genParam({ team: MANU })).lastSquad() as Log;
        if (Date.now() > lastSquadLog.datetime.getTime() + timeS.d && now.getDay() == 5) {
            await fetchSquad({ team: MANU });
        }
        
        const lastCoachhLog = await Log.where("param", genParam({ team: MANU })).lastPlayer() as Log;
        if (Date.now() > lastCoachhLog.datetime.getTime() + timeS.w * 2) {
            await fetchCoach({ team: MANU });
        }
    }

    if (apiRemain > 0) {
        const seasons = await Season.orderBy("year", "desc").get() as Season[];
        for (let i = 0; i < seasons.length; i++) {
            if (apiRemain == 0) break;
            const _s = seasons[i];
            let matchs = await Match.where("seasonCode", _s.code).where("date", "<", new Date()).orderBy("date", "desc").get() as Match[];
            if (matchs.length == 0) {
                await fetchMatch({ team: MANU, season: _s.year });
                matchs = await Match.where("seasonCode", _s.code).orderBy("date", "desc").get() as Match[];
            }
            for (let j = 0; j < matchs.length; j++) {
                if (apiRemain == 0) break;
                const _m = matchs[j];
                if (matchStatus[_m.status] > 10) {
                    if (apiRemain == 0) break;
                    if ((await Match.where("id", _m.id).heads() as Head[]).length < 2) await fetchLineup({ fixture: _m.id });
                    if (apiRemain == 0) break;
                    if (!(await Log.where("param", genParam({ fixture: _m.id })).lastIncident())) await fetchIncident({ fixture: _m.id });
                    if (apiRemain == 0) break;
                    if (!(await Log.where("param", genParam({ fixture: _m.id })).lastMatchStats())) await fetchMatchStatistics({ fixture: _m.id });
                }
            }
        }
    }

    setTimeout(continueFetch, timeS.h);
}

/*async function liveFetch(matchId: number) {
    const match = await Match.find(matchId) as Match;
    if (matchStatus[match.status] > 10 || apiRemain == 0) {
        liveId = null;
        continueFetch();
        return;
    }
    const league = await Season.where("code", match.seasonCode).league() as League;
    await fetchMatch({ league: league.id, live: "all" });

    liveId = setTimeout(liveFetch, timeS.m * (1.21 + Math.random() * 0.05), matchId);
}*/