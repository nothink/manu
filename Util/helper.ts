export let apiRemain = 100;

export function setApiRemain(remain: number) {
    apiRemain = remain;
}

export function displayDate(date: Date, language: string): string {
    const tz = localStorage.getItem("tz") ?? "Asia/Hong_Kong";
    return new Date(date).toLocaleString(language??"en-US", { timeZone: tz, weekday: "short", year: "numeric", month: "2-digit", day: "2-digit", hour12: false, hour: "2-digit", minute: "2-digit" });
}

export function trySeason(): number {
    const date = new Date();
    return date.getFullYear() - (date.getMonth() < 7 ? 1 : 0);
}

export function genParam(json: {[k: string]: string | number | boolean}): string {
    let first = true;
    let params = "";
    for (const key in json) {
        params += (first ? '?' : '&') + key + '=' + json[key].toString();
        first = false;
    }
    return params;
}

export function genTLA(name: string): string {
    const nS = name.replace(" FC", "").split(" ");

    let tla = "";
    switch (nS.length) {
        case 1:
            tla = nS[0].substr(0, 3);
            break;
        case 2:
            tla = nS[0][0] + nS[1].substr(0, 2);
            break;
        case 3:
            tla = nS[0][0] + nS[1][0] + nS[2][0]
            break
    }
    return tla.toUpperCase();
}

export function getTimeToNextWeekDay(day: number, cur?: Date): number {
    if (!cur) cur = new Date();
    return cur.setDate(cur.getDate() + (day - cur.getDay() + 7) % 7);
}