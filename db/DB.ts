import "https://deno.land/x/dotenv/load.ts";
import { Database, Relationships, SQLite3Connector } from 'https://deno.land/x/denodb/mod.ts';
import { Coach } from "../Model/Coach.ts";
import { Incident } from "../Model/Incident.ts";
import { Head } from "../Model/Head.ts";
import { League } from "../Model/League.ts";
import { Lineup } from "../Model/Lineup.ts";
import { Log } from "../Model/Log.ts";
import { Match } from "../Model/Match.ts";
import { Player } from "../Model/Player.ts";
import { Season } from "../Model/Season.ts";
import { Standing } from "../Model/Standing.ts";
import { Team } from "../Model/Team.ts";

const db = new Database(new SQLite3Connector({
  filepath: Deno.env.get("DB") as string,
}));

Relationships.belongsTo(Season, League);
Relationships.belongsTo(Standing, Team);
Relationships.belongsTo(Player, Team);
Relationships.belongsTo(Coach, Team);
Relationships.belongsTo(Head, Team);
Relationships.belongsTo(Standing, Season, { foreignKey: "seasonCode" });
Relationships.belongsTo(Match, Season, { foreignKey: "seasonCode" });
Relationships.belongsTo(Lineup, Player);
Relationships.belongsTo(Head, Coach);
Relationships.belongsTo(Head, Match);
Relationships.belongsTo(Incident, Match);
Relationships.belongsTo(Lineup, Head, { foreignKey: "headCode" });
Relationships.belongsTo(Incident, Lineup, { foreignKey: "mainCode" });
Relationships.belongsTo(Incident, Lineup, { foreignKey: "subCode" });

db.link([Log, League, Coach, Season, Team, Standing, Player, Match, Head, Lineup, Incident]);