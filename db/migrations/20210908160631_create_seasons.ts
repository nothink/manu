import { AbstractMigration, Info, ClientSQLite } from "https://deno.land/x/nessie/mod.ts";
import { DBDialects, Schema } from "https://raw.githubusercontent.com/halvardssm/deno-query-builder/main/mod.ts";
import { tableName } from "../../Model/Season.ts";

export default class extends AbstractMigration<ClientSQLite> {
  async up({ dialect }: Info): Promise<void> {
    const schema = new Schema("sqlite3" as DBDialects);
    schema.create(
      tableName,
      (table: any) => {
        table.string("code").primary();
        table.bigInteger("league_id").notNullable(); //leagues
        table.smallInteger("year").unsigned().notNullable();
        table.date("start");
        table.date("end");
        table.boolean("is_cur").notNullable().default(false);
        table.custom("FOREIGN KEY(league_id) REFERENCES leagues(id)");
      },
    );

    await this.client.query(schema.query.toString());


  }

  async down({ dialect }: Info): Promise<void> {
    await this.client.query(new Schema("sqlite3" as DBDialects).drop(tableName).toString());
  }
}