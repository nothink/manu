import { AbstractMigration, Info, ClientSQLite } from "https://deno.land/x/nessie/mod.ts";
import { DBDialects, Schema } from "https://raw.githubusercontent.com/halvardssm/deno-query-builder/main/mod.ts";
import { tableName } from "../../Model/Lineup.ts";

export default class extends AbstractMigration<ClientSQLite> {
  async up({ dialect }: Info): Promise<void> {
    const schema = new Schema("sqlite3" as DBDialects);
    schema.create(
      tableName,
      (table: any) => {
        table.string("code").primary();
        table.string("head_code").notNullable(); //heads
        table.bigInteger("player_id").notNullable(); //players
        table.smallInteger("no").notNullable();
        table.string("pos").notNullable();
        table.string("grid");
        table.smallInteger("in").unsigned();
        table.json("stats");
        table.custom("FOREIGN KEY(head_code) REFERENCES heads(code)");
        table.custom("FOREIGN KEY(player_id) REFERENCES players(id)");
      },
    );

    await this.client.query(schema.query.toString());
  }

  async down({ dialect }: Info): Promise<void> {
    await this.client.query(new Schema("sqlite3" as DBDialects).drop(tableName).toString());
  }
}