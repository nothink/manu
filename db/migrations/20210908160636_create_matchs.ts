import { AbstractMigration, Info, ClientSQLite } from "https://deno.land/x/nessie/mod.ts";
import { DBDialects, Schema } from "https://raw.githubusercontent.com/halvardssm/deno-query-builder/main/mod.ts";
import { matchStatus, tableName } from "../../Model/Match.ts";

export default class extends AbstractMigration<ClientSQLite> {
  async up({ dialect }: Info): Promise<void> {
    const schema = new Schema("sqlite3" as DBDialects);
    schema.create(
      tableName,
      (table: any) => {
        table.id();
        table.string("season_code").notNullable(); //seasons
        table.dateTime("date");
        table.enum("status", Object.keys(matchStatus)).notNullable();
        table.json("score");
        table.custom("FOREIGN KEY(season_code) REFERENCES seasons(code)");
      },
    );

    await this.client.query(schema.query.toString());
  }

  async down({ dialect }: Info): Promise<void> {
    await this.client.query(new Schema("sqlite3" as DBDialects).drop(tableName).toString());
  }
}