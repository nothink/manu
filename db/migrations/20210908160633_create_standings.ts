import { AbstractMigration, Info, ClientSQLite } from "https://deno.land/x/nessie/mod.ts";
import { DBDialects, Schema } from "https://raw.githubusercontent.com/halvardssm/deno-query-builder/main/mod.ts";
import { tableName } from "../../Model/Standing.ts";

export default class extends AbstractMigration<ClientSQLite> {
  async up({ dialect }: Info): Promise<void> {
    const schema = new Schema("sqlite3" as DBDialects);
    schema.create(
      tableName,
      (table: any) => {
        table.string("code").primary();
        table.bigInteger("season_code").notNullable(); //seasons
        table.bigInteger("team_id").notNullable(); //teams
        table.smallInteger("points").unsigned().notNullable().default(0);
        table.smallInteger("played").unsigned().notNullable().default(0);
        table.smallInteger("win").unsigned().notNullable().default(0);
        table.smallInteger("draw").unsigned().notNullable().default(0);
        table.smallInteger("lose").unsigned().notNullable().default(0);
        table.smallInteger("goals").unsigned().notNullable().default(0);
        table.smallInteger("be_goal").unsigned().notNullable().default(0);
        table.smallInteger("goals_diff").notNullable().default(0);
        table.jsonb("home").notNullable();
        table.jsonb("away").notNullable();
        table.custom("FOREIGN KEY(season_code) REFERENCES seasons(code)");
        table.custom("FOREIGN KEY(team_id) REFERENCES teams(id)");
      },
    );

    await this.client.query(schema.query.toString());
  }

  async down({ dialect }: Info): Promise<void> {
    await this.client.query(new Schema("sqlite3" as DBDialects).drop(tableName).toString());
  }
}