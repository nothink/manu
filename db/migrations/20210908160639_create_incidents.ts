import { AbstractMigration, Info, ClientSQLite } from "https://deno.land/x/nessie/mod.ts";
import { DBDialects, Schema } from "https://raw.githubusercontent.com/halvardssm/deno-query-builder/main/mod.ts";
import { incidentType, tableName } from "../../Model/Incident.ts";

export default class extends AbstractMigration<ClientSQLite> {
  async up({ dialect }: Info): Promise<void> {
    const schema = new Schema("sqlite3" as DBDialects);
    schema.create(
      tableName,
      (table: any) => {
        table.string("code").primary();
        table.bigInteger("match_id").notNullable(); //matchs
        table.smallInteger("seq").unsigned().notNullable();
        table.smallInteger("time").unsigned().notNullable();
        table.smallInteger("extra").unsigned().notNullable();
        table.bigInteger("main_code").notNullable(); //lineups
        table.bigInteger("sub_code"); //lineups
        table.enum("type", Object.keys(incidentType)).notNullable();
        table.string("detail").notNullable();
        table.custom("FOREIGN KEY(match_id) REFERENCES matchs(id)");
        table.custom("FOREIGN KEY(main_code) REFERENCES lineups(code)");
        table.custom("FOREIGN KEY(sub_code) REFERENCES lineups(code)");
      },
    );

    await this.client.query(schema.query.toString());
  }

  async down({ dialect }: Info): Promise<void> {
    await this.client.query(new Schema("sqlite3" as DBDialects).drop(tableName).toString());
  }
}