import { AbstractMigration, Info, ClientSQLite } from "https://deno.land/x/nessie/mod.ts";
import { DBDialects, Schema } from "https://raw.githubusercontent.com/halvardssm/deno-query-builder/main/mod.ts";
import { tableName } from "../../Model/Head.ts";

export default class extends AbstractMigration<ClientSQLite> {
  async up({ dialect }: Info): Promise<void> {
    const schema = new Schema("sqlite3" as DBDialects);
    schema.create(
      tableName,
      (table: any) => {
        table.string("code").primary();
        table.bigInteger("match_id").notNullable(); //matchs
        table.bigInteger("team_id").notNullable(); //teams
        table.boolean("is_home").notNullable();
        table.bigInteger("coach_id"); //coachs
        table.string("form");
        table.custom("FOREIGN KEY(match_id) REFERENCES matchs(id)");
        table.custom("FOREIGN KEY(team_id) REFERENCES teams(id)");
        table.custom("FOREIGN KEY(coach_id) REFERENCES coachs(id)");
      },
    );

    await this.client.query(schema.query.toString());
  }

  async down({ dialect }: Info): Promise<void> {
    await this.client.query(new Schema("sqlite3" as DBDialects).drop(tableName).toString());
  }
}