#!/bin/bash
[ -n .env ] && db=$(awk -F= '/DB/ {print $2}' .env) || exit 1
[ -z db ] && echo "please set db location" && exit 2

clear

bash migrate.sh migrate

deno run \
    --allow-env \
    --allow-net=:80,v3.football.api-sports.io \
    --allow-read=.env,.env.defaults,.env.example,$db,${db}-journal,Page/ \
    --allow-write=$db,${db}-journal \
    --unstable \
    --watch \
    main.ts