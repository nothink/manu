import { Season } from "../Model/Season.ts";
import { Standing } from "../Model/Standing.ts";
import { Team } from "../Model/Team.ts";
import { React } from "../ssr.ts";
import { PREMIER_LEAGUE } from "../Util/const.ts";

const teamO: {[key: number]: {name: string, tla: string, logo: string}} = {};
(await Team.all() as Team[]).map(t => teamO[t.id] = {name: t.name, tla: t.tla, logo: t.logo});

const curSeason = await Season.curSeason(PREMIER_LEAGUE) as Season;
let standings =  await Standing.where("seasonCode", curSeason.code).get() as Standing[];

const Row = (props: any) => 
    <tr>
        <td><img className="teamIcon" src={teamO[props.teamId].logo} /> {teamO[props.teamId].name}</td>
        <td className="number">{props.played}</td>
        <td className="number">{props.win}</td>
        <td className="number">{props.draw}</td>
        <td className="number">{props.lose}</td>
        <td className="number">{props.points}</td>
        <td className="vs">{props.goals} - {props.beGoal}</td>
        <td className="number">{props.goalsDiff}</td>
    </tr>;

export const StandingFE = () => {
    return (
        <table className="standing">
            <thead>
                <th>Team</th>
                <th>場</th>
                <th>勝</th>
                <th>和</th>
                <th>負</th>
                <th>分</th>
                <th>+/-</th>
                <th>差</th>
            </thead>
            {standings.map((standing: Standing) => (
            <Row key={standing.code} {...standing} />
            ))}
        </table>
    )
}

async function fetching() {
    standings =  await Standing.where("seasonCode", curSeason.code).get() as Standing[];
    setTimeout(fetching, 3000);
}
fetching();