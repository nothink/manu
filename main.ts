import "./db/DB.ts";
import { Application, send, Context, isHttpError, Status } from "https://deno.land/x/oak/mod.ts";
import { initialFetch } from "./Util/cron.ts";
import { router } from "./route.ts";

initialFetch();

const app = new Application();

app.use(async (ctx: Context, next: any) => {
    const end = ctx.request.url.pathname;
    if (end.substr(end.lastIndexOf(".")) == ".css") {
        await send(ctx, end, {
            root: "Page/css",
        });
    } else await next();
});

app.use(async (ctx: Context, next: any) => {
    try {
        await next();
    } catch (err) {
        if (isHttpError(err)) {
            switch (err.status) {
                case Status.NotFound:
                    ctx.response.status = Status.NotFound;
                break;
                default:
                    ctx.response.redirect("/");
                break;
            }
        } else {
        // rethrow if you can't handle the error
            throw err;
        }
    }
});

app.use(router.routes());
app.use(router.allowedMethods());

await app.listen({ port: 80 });