import {
    ClientSQLite,
    NessieConfig,
} from "https://deno.land/x/nessie/mod.ts";
import { config as env } from "https://deno.land/x/dotenv/mod.ts";

const client = new ClientSQLite(env().DB);

/** This is the final config object */
const config: NessieConfig = {
    client,
    migrationFolders: ["./db/migrations"],
    seedFolders: ["./db/seeds"],
};

export default config;

