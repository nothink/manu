#!/bin/bash
[ -n .env ] && db=$(awk -F= '/DB/ {print $2}' .env) || exit 1
[ -z db ] && echo "please set db location" && exit 2
par=""
if [ $# -eq 2 ]; then
  [[ $1 =~ ^(migrate|rollback)$ ]] && action=$1
  [[ $2 =~ ^([0-9]+|all)$ ]] && par=$2
elif [ $# -eq 1 ]; then
  [[ $1 =~ ^(migrate|rollback)$ ]] && action=$1

  if [[ $1 =~ ^-?([0-9]+|all)?$ ]]; then
    [[ $1 =~ ^- ]] && action=rollback || action=migrate
    par=${1#-}
  fi
fi
[ -z $action ] && exit 3
deno run \
    --allow-net=deno.land,raw.githubusercontent.com,cdn.skypack.dev \
    --allow-read \
    --allow-write=$db,${db}-journal \
    --unstable \
    https://deno.land/x/nessie/cli.ts $action $par