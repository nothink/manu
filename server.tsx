import { Context, RouterContext } from "https://deno.land/x/oak/mod.ts";
import { React } from "./ssr.ts";
import { ReactDOMServer } from "./ssr.ts";
import { App } from "./Page/App.tsx";

export function getIndex(ctx: Context) {
  ctx.response.body = `
  <!DOCTYPE html>
  <html>
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="style.css">
    <link rel="stylesheet" href="standing.css">
  </head>
  <body>
    <div id="root">
    ${ReactDOMServer.renderToString(<App />)}
    </div>
  </body>
  </html>`;
}