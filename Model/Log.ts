import { Model, DataTypes } from "https://deno.land/x/denodb/mod.ts";

export const tableName = "logs";

export class Log extends Model {
    static table = tableName;

    id!: number;
    datetime!: Date;
    url!: string;
    endpoint!: string;
    param!: string;
    isError!: boolean;
    info!: string;

    static fields = {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
        },

        datetime: DataTypes.DATETIME,

        url: DataTypes.STRING,

        endpoint: DataTypes.STRING,

        param: DataTypes.STRING,

        isError: DataTypes.BOOLEAN,

        info: DataTypes.STRING,
    }

    static async lastLeague(isError = false) {
        return await this.where({endpoint: "leagues", isError: isError}).orderBy("datetime", "desc").first();
    }

    static async lastTeam(isError = false) {
        return await this.where({endpoint: "teams", isError: isError}).orderBy("datetime", "desc").first();
    }

    static async lastStanding(isError = false) {
        return await this.where({endpoint: "standings", isError: isError}).orderBy("datetime", "desc").first();
    }

    static async lastPlayer(isError = false) {
        return await this.where({endpoint: "players", isError: isError}).orderBy("datetime", "desc").first();
    }

    static async lastCoach(isError = false) {
        return await this.where({endpoint: "coachs", isError: isError}).orderBy("datetime", "desc").first();
    }

    static async lastSquad(isError = false) {
        return await this.where({endpoint: "players/squads", isError: isError}).orderBy("datetime", "desc").first();
    }

    static async lastMatch(isError = false) {
        return await this.where({endpoint: "fixtures", isError: isError}).orderBy("datetime", "desc").first();
    }

    static async lastLineup(isError = false) {
        return await this.where({endpoint: "fixtures/lineups", isError: isError}).orderBy("datetime", "desc").first();
    }

    static async lastIncident(isError = false) {
        return await this.where({endpoint: "fixtures/events", isError: isError}).orderBy("datetime", "desc").first();
    }

    static async lastMatchStats(isError = false) {
        return await this.where({endpoint: "fixtures/players", isError: isError}).orderBy("datetime", "desc").first();
    }
}