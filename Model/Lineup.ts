import { Model, DataTypes } from "https://deno.land/x/denodb/mod.ts";
import { Head } from "./Head.ts";
import { Incident } from "./Incident.ts";
import { Player } from "./Player.ts";

export const tableName = "lineups";

export class Lineup extends Model {
    static table = tableName;

    code!: string;
    headCode!: string;
    playerId!: string;
    no!: number;
    pos!: string;
    grid!: string;
    in!: number;
    stats!: string;

    static fields = {
        code: {
            type: DataTypes.STRING,
            primaryKey: true,
        },

        no: DataTypes.INTEGER,

        pos: DataTypes.STRING,

        grid: DataTypes.STRING,

        in: DataTypes.INTEGER,

        stats: DataTypes.JSONB,
    }

    static head() { return this.hasOne(Head); }

    static player() { return this.hasOne(Player); }

    static evMains() { return this.hasMany(Incident); }

    static evSubs() { return this.hasMany(Incident); }
}