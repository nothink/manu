import { Model, DataTypes } from "https://deno.land/x/denodb/mod.ts";
import { Coach } from "./Coach.ts";
import { Head } from "./Head.ts";
import { Player } from "./Player.ts";
import { Standing } from "./Standing.ts";

export const tableName = "teams";

export class Team extends Model {
    static table = tableName;

    id!: number;
    name!: string;
    tla!: string;
    logo!: string;

    static fields = {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
        },

        name: DataTypes.STRING,

        tla: DataTypes.STRING,

        logo: DataTypes.STRING,
    }

    static standings() { return this.hasMany(Standing); }

    static players() { return this.hasMany(Player); }

    static coachs() { return this.hasMany(Coach); }

    static heads() { return this.hasMany(Head); }
}