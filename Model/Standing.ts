import { Model, DataTypes } from "https://deno.land/x/denodb/mod.ts";
import { Season } from "./Season.ts";
import { Team } from "./Team.ts";

export const tableName = "standings";

export class Standing extends Model {
    static table = tableName;

    code!: string;
    seasonCode!: string;
    teamId!: number;
    points!: number;
    played!: number;
    win!: number;
    draw!: number;
    lose!: number;
    goals!: number;
    beGoal!: number;
    goalsDiff!: number;
    home!: string;
    away!: string;

    static fields = {
        code: {
            type: DataTypes.STRING,
            primaryKey: true,
        },

        points: DataTypes.INTEGER,

        played: DataTypes.INTEGER,

        win: DataTypes.INTEGER,

        draw: DataTypes.INTEGER,

        lose: DataTypes.INTEGER,

        goals: DataTypes.INTEGER,

        beGoal: DataTypes.INTEGER,

        goalsDiff: DataTypes.INTEGER,

        home: DataTypes.JSONB,

        away: DataTypes.JSONB,
    }

    static season() { return this.hasOne(Season); }

    static team() { return this.hasOne(Team); }
}