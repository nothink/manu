import { Model, DataTypes } from "https://deno.land/x/denodb/mod.ts";
import { Season } from "./Season.ts";

export const tableName = "leagues";

export class League extends Model {
    static table = tableName;

    id!: number;
    name!: string;
    logo!: string;

    static fields = {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
        },

        name: DataTypes.STRING,

        logo: DataTypes.STRING,
    }

    static seasons() { return this.hasMany(Season); }
}