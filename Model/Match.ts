import { Model, DataTypes } from "https://deno.land/x/denodb/mod.ts";
import { Head } from "./Head.ts";
import { Incident } from "./Incident.ts";
import { Season } from "./Season.ts";

export enum MATCH_STATUS {
    "TBD" = -1,
    "NS" = 0,
    "1H" = 1,
    "HT" = 1.5,
    "2H" = 1,
    "ET" = 2,
    "P" = 3,
    "FT" = 11,
    "AET" = 12,
    "PEN" = 13,
    "BT" = 2.5,
    "SUSP" = -2,
    "INT" = -3,
    "PST" = -4,
    "CANC" = -11,
    "ABD" = -12,
    "AWD" = -101,
    "WO" = 15,
    "LIVE" = 10,
};

export const matchStatus: Record<string, MATCH_STATUS> = {
    "TBD": MATCH_STATUS.TBD,
    "NS": MATCH_STATUS.NS,
    "1H": MATCH_STATUS["1H"],
    "HT": MATCH_STATUS.HT,
    "2H": MATCH_STATUS["2H"],
    "ET": MATCH_STATUS.ET,
    "P": MATCH_STATUS.P,
    "FT": MATCH_STATUS.FT,
    "AET": MATCH_STATUS.AET,
    "PEN": MATCH_STATUS.PEN,
    "BT": MATCH_STATUS.BT,
    "SUSP": MATCH_STATUS.SUSP,
    "INT": MATCH_STATUS.INT,
    "PST": MATCH_STATUS.PST,
    "CANC": MATCH_STATUS.CANC,
    "ABD": MATCH_STATUS.ABD,
    "AWD": MATCH_STATUS.AWD,
    "WO": MATCH_STATUS.WO,
    "LIVE": MATCH_STATUS.LIVE,
};

export const tableName = "matchs";

export class Match extends Model {
    static table = tableName;

    id!: number;
    seasonCode!: string;
    date!: Date;
    status!: MATCH_STATUS;
    score!: string;

    static fields = {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
        },

        date: DataTypes.DATE,

        status: DataTypes.enum(Object.keys(matchStatus)),

        score: DataTypes.JSON,
    }

    static season() { return this.hasOne(Season); }

    static heads() { return this.hasMany(Head); }

    static incidents() { return this.hasMany(Incident); }
}