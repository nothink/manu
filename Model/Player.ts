import { Model, DataTypes } from "https://deno.land/x/denodb/mod.ts";
import { Team } from "./Team.ts";

export const tableName = "players";

export class Player extends Model {
    static table = tableName;

    id!: number;
    name!: string;
    first!: string;
    last!: string;
    age!: number;
    dob!: Date;
    country!: string;
    nationality!: string;
    height!: number;
    weight!: number;
    injure!: boolean;
    photo!: string;
    teamId!: number;
    no!: number;
    pos!: string;

    static fields = {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
        },

        name: DataTypes.STRING,

        first: DataTypes.STRING,

        last: DataTypes.STRING,

        age: DataTypes.INTEGER,

        dob: DataTypes.DATE,

        country: DataTypes.STRING,

        nationality: DataTypes.STRING,

        height: DataTypes.INTEGER,

        weight: DataTypes.INTEGER,

        injure: DataTypes.BOOLEAN,

        photo: DataTypes.STRING,

        no: DataTypes.INTEGER,

        pos: DataTypes.STRING,
    }

    static team() { return this.hasOne(Team); }
}