import { Model, DataTypes } from "https://deno.land/x/denodb/mod.ts";
import { Coach } from "./Coach.ts";
import { Lineup } from "./Lineup.ts";
import { Match } from "./Match.ts";
import { Team } from "./Team.ts";

export const tableName = "heads";

export class Head extends Model {
    static table = tableName;

    code!: string;
    matchId!: number;
    teamId!: number;
    isHome!: boolean;
    coachId!: number;
    form!: string;

    static fields = {
        code: {
            type: DataTypes.STRING,
            primaryKey: true,
        },

        isHome: DataTypes.BOOLEAN,
        
        form: DataTypes.STRING,
    }

    static match() { return this.hasOne(Match); }

    static team() { return this.hasOne(Team); }

    static coach() { return this.hasOne(Coach); }

    static lineups() { return this.hasMany(Lineup); }
}