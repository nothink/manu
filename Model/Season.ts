import { Model, DataTypes } from "https://deno.land/x/denodb/mod.ts";
import { League } from "./League.ts";
import { Match } from "./Match.ts";
import { Standing } from "./Standing.ts";

export const tableName = "seasons";

export class Season extends Model {
    static table = tableName;

    code!: string;
    leagueId!: number;
    year!: number;
    start!: Date;
    end!: Date;
    isCur!: boolean;

    static fields = {
        code: {
            type: DataTypes.STRING,
            primaryKey: true,
        },

        year: DataTypes.INTEGER,

        start: DataTypes.DATE,

        end: DataTypes.DATE,

        isCur: DataTypes.BOOLEAN,
    }

    static league() { return this.hasOne(League); }

    static standings() { return this.hasMany(Standing); }

    static matchs() { return this.hasMany(Match); }

    static async curSeason(leagueId?: number) {
        const query = this.where("isCur", true);
        if (leagueId) query.where("leagueId", leagueId);
        else query.orderBy("year", "desc");
        return await query.first();
    }
}