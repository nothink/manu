import { Model, DataTypes } from "https://deno.land/x/denodb/mod.ts";
import { Head } from "./Head.ts";
import { Team } from "./Team.ts";

export const tableName = "coachs";

export class Coach extends Model {
    static table = tableName;

    id!: number;
    name!: string;
    first!: string;
    last!: string;
    age!: number;
    dob!: Date;
    country!: string;
    nationality!: string;
    photo!: string;
    teamId!: number;

    static fields = {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
        },

        name: DataTypes.STRING,

        first: DataTypes.STRING,

        last: DataTypes.STRING,

        age: DataTypes.INTEGER,

        dob: DataTypes.DATE,

        country: DataTypes.STRING,

        nationality: DataTypes.STRING,

        photo: DataTypes.STRING,
    }

    static team() { return this.hasOne(Team); }

    static heads() { return this.hasMany(Head); }
}