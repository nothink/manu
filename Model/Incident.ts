import { Model, DataTypes } from "https://deno.land/x/denodb/mod.ts";
import { Lineup } from "./Lineup.ts";
import { Match } from "./Match.ts";

export enum INCIDENT_TYPE {
    "Normal Goal" = "Goal",
    "Own Goal" = "Goal",
    "Penalty" = "Goal",
    "Missed Penalty" = "Goal",
    "Yellow Card" = "Card",
    "Second Yellow Card" = "Card",
    "Red Card" = "Card",
    "Substitution 1" = "subst",
    "Substitution 2" = "subst",
    "Substitution 3" = "subst",
    "Goal cancelled" = "Var",
    "Penalty confirmed" = "Var",
};

export const incidentType = {
    "Normal Goal": INCIDENT_TYPE["Normal Goal"],
    "Own Goal": INCIDENT_TYPE["Own Goal"],
    "Penalty": INCIDENT_TYPE["Penalty"],
    "Missed Penalty": INCIDENT_TYPE["Missed Penalty"],
    "Yellow Card": INCIDENT_TYPE["Yellow Card"],
    "Second Yellow Card": INCIDENT_TYPE["Second Yellow Card"],
    "Red Card": INCIDENT_TYPE["Red Card"],
    "Substitution 1": INCIDENT_TYPE["Substitution 1"],
    "Substitution 2": INCIDENT_TYPE["Substitution 2"],
    "Substitution 3": INCIDENT_TYPE["Substitution 3"],
    "Goal cancelled": INCIDENT_TYPE["Goal cancelled"],
    "Penalty confirmed": INCIDENT_TYPE["Penalty confirmed"],
};

export const tableName = "incidents";

export class Incident extends Model {
    static table = tableName;

    code!: string;
    matchId!: number;
    seq!: number;
    time!: number;
    extra!: number;
    mainCode!: string;
    subCode!: string;
    type!: INCIDENT_TYPE;
    detail!: INCIDENT_TYPE;

    static fields = {
        code: {
            type: DataTypes.STRING,
            primaryKey: true,
        },

        seq: DataTypes.INTEGER,

        time: DataTypes.INTEGER,

        extra: DataTypes.INTEGER,

        type: DataTypes.enum(["Goal", "Card", "subst", "Var"]),

        detail: DataTypes.enum(Object.keys(incidentType)),
    }

    static match() { return this.hasOne(Match); }

    static main() { return this.hasOne(Lineup); }

    static sub() { return this.hasOne(Lineup); }
}