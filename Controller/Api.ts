import { API_URL, FETCH_INIT } from "../Util/const.ts";
import { genParam, genTLA, setApiRemain, trySeason } from "../Util/helper.ts";
import { League } from "../Model/League.ts";
import { Log } from "../Model/Log.ts";
import { Season } from "../Model/Season.ts";
import { Team } from "../Model/Team.ts";
import { Standing } from "../Model/Standing.ts";
import { Player } from "../Model/Player.ts";
import { Coach } from "../Model/Coach.ts";
import { Match } from "../Model/Match.ts";
import { Head } from "../Model/Head.ts";
import { Lineup } from "../Model/Lineup.ts";
import { Incident } from "../Model/Incident.ts";
import { ParamCoach, ParamIncident, ParamLeague, ParamLineup, ParamMatch, ParamMatchStats, ParamPlayer, ParamSquad, ParamStanding, ParamTeam } from "../Util/types.ts";

async function fetchApi(endpoint: string, param = "", retry = 1, log?: Log): Promise<{[key: string]: any}> {
    if (!log) {
        log = new Log();
        log.url = API_URL;
        log.endpoint = endpoint;
        log.param = param;
    }
    log.datetime = new Date();
    try {
        const response = await fetch("https://" + API_URL + "/" + endpoint + param, FETCH_INIT);
        const json = await response.json();
        if (response.headers.get("x-ratelimit-requests-remaining")) setApiRemain(Number(response.headers.get("x-ratelimit-requests-remaining")))
        
        if (response.status == 200 && (!json.errors || Object.keys(json.errors).length == 0)) {
            const errorKey = Object.keys(json.errors);
            if (errorKey.length > 0) {
                log.isError = true;
                for (let i = 0; i < errorKey.length; i++) {
                    const _eK = errorKey[i];
                    log.info = _eK + ": " + json.errors[_eK] + (i < errorKey.length - 1 ? "\n" : "");
                }
            }
            else log.isError = false;
            await log.save();
            
            return json;
        } else {
            let error = "[" + retry + "]: ";
            if (json.message) error += json.message;
            else if (Object.keys(json.errors).length > 0) error += JSON.stringify(json.errors);
            else error += response.statusText;

            throw new Error((retry != 1 ? "\n" : "") + error);
        }
    } catch(e) {
        log.info += e.message;
        if (retry == 3) {
            log.isError = true;
            await log.save();
            return { error: e.message };
        }
        await new Promise(s => setTimeout(s, 3000));
        return await fetchApi(endpoint, param, retry + 1, log);
    }
}

export async function fetchApiStatu(retry = 1) {
    try {
        const response = await fetch("https://" + API_URL + "/status", FETCH_INIT);
        const json = await response.json();

        if (response.status == 200) setApiRemain(json.response.requests.current);
        else throw new Error("");
    } catch(e) {
        await new Promise(s => setTimeout(s, 5000));
        await fetchApiStatu(retry + 1);
    }
}

export async function fetchLeague(params: ParamLeague = {}) {
    try {
        const json = await fetchApi("leagues", genParam(params));
        
        for (const res of json.response) {
            const _l = res.league;

            const league = await League.find(_l.id) ?? new League();
            league.name = _l.name;
            league.logo = _l.logo;

            if (league.id) await league.update();
            else {
                league.id = _l.id;
                await league.save();
            }

            for (const _s of res.seasons) {
                const code = _l.id + "-" + _s.year;
                const season = await Season.find(code) ?? new Season();
                season.leagueId = _l.id;
                season.year = _s.year;
                season.start = new Date(_s.start);
                season.end = new Date(_s.end);
                season.isCur = _s.current;

                if (season.code) await season.update();
                else {
                    season.code = code;
                    await season.save();
                }
            }
        }
    } catch(e) {
        console.error(e);
    }
}

export async function fetchTeam(params: ParamTeam) {
    try {
        const json = await fetchApi("teams", genParam(params));
        
        for (const res of json.response) {
            const _t = res.team;

            const team = await Team.find(_t.id) ?? new Team();
            team.name = _t.name;
            team.tla = genTLA(_t.name);
            team.logo = _t.logo;

            if (team.id) await team.update();
            else {
                team.id = _t.id;
                await team.save();
            }
        }
    } catch(e) {
        console.error(e);
    }
}

export async function fetchStanding(params: ParamStanding) {
    try {
        const json = await fetchApi("standings", genParam(params));
        
        for (const res of json.response) {
            const _l = res.league;
            const season = await checkSeasonExist(_l.id + "-" + _l.season, _l);
            
            for (const _ss of _l.standings) {
                for (const _s of _ss) {
                    const team = await checkTeamExist(_s.team.id, _s.team);

                    const home: {[key: string] : number} = {};
                    const away: {[key: string] : number} = {};
                    const code = season.code + "-" + team.id;
                    const standing = await Standing.find(code) ?? new Standing();
                    standing.seasonCode = season.code;
                    standing.teamId = team.id;
                    standing.points = _s.points;
                    for (const _k of ["played", "win", "draw", "lose"]) {
                        standing[_k] = _s.all[_k];
                        home[_k] = _s.home[_k];
                        away[_k] = _s.away[_k];
                    }
                    standing.goals = _s.all.goals.for;
                    standing.beGoal = _s.all.goals.against;
                    standing.goalsDiff = _s.goalsDiff;
                    home.goals = _s.home.goals.for;
                    home.beGoal = _s.home.goals.against;
                    home.goalsDiff = _s.home.goals.for - _s.home.goals.against;
                    away.goals = _s.away.goals.for;
                    away.beGoal = _s.away.goals.against;
                    away.goalsDiff = _s.away.goals.for - _s.away.goals.against;
                    standing.home = JSON.stringify(home);
                    standing.away = JSON.stringify(away);

                    if (standing.code) await standing.update();
                    else {
                        standing.code = code;
                        await standing.save();
                    }
                }
            }
        }
    } catch(e) {
        console.error(e);
    }
}

export async function fetchPlayer(params: ParamPlayer) {
    try {
        const json = await fetchApi("players", genParam(params));
        
        for (const res of json.response) {
            const _p = res.player;

            const player = await Player.find(_p.id) ?? new Player();
            player.name = _p.name;
            player.first = _p.firstname;
            player.last = _p.lastname;
            player.age = _p.age;
            player.dob = new Date(_p.birth.date);
            player.country = _p.birth.country;
            player.nationality = _p.nationality;
            player.height = _p.height.replace(/[^\d]+/, "");
            player.weight = _p.weight.replace(/[^\d]+/, "");
            player.injure = _p.injure;
            player.photo = _p.photo;

            if (player.id) await player.update();
            else {
                player.id = _p.id;
                await player.save();
            }
        }

        if (json.paging.current < json.paging.total) {
            params.page = json.paging.current + 1;
            await fetchPlayer(params);
        }
    } catch(e) {
        console.error(e);
    }
}

export async function fetchSquad(params: ParamSquad) {
    try {
        const json = await fetchApi("players/squads", genParam(params));
        
        for (const res of json.response) {
            const team = await checkTeamExist(res.team.id, res.team);

            for (const _p of res.players) {
                const player = await Player.find(_p.id) ?? new Player();

                player.teamId = team.id;
                player.name = _p.name;
                player.age = _p.age;
                player.photo = _p.photo;
                player.no = _p.number;
                player.pos = _p.position;

                if (player.id) await player.update();
                else {
                    player.id = _p.id;
                    await player.save();
                }
            }
        }
    } catch(e) {
        console.error(e);
    }
}

export async function fetchCoach(params: ParamCoach) {
    try {
        const json = await fetchApi("coachs", genParam(params));
        
        for (const res of json.response) {
            const _c = res;

            const coach = await Coach.find(_c.id) ?? new Coach();
            coach.name = _c.name;
            coach.first = _c.firstname;
            coach.last = _c.lastname;
            coach.age = _c.age;
            coach.dob = new Date(_c.birth.date);
            coach.country = _c.birth.country;
            coach.nationality = _c.nationality;
            coach.photo = _c.photo;
            if (_c.team) {
                const team = await checkTeamExist(_c.team.id, _c.team);
                coach.teamId = team.id;
            }

            if (coach.id) await coach.update();
            else {
                coach.id = _c.id;
                await coach.save();
            }
        }
    } catch(e) {
        console.error(e);
    }
}

export async function fetchMatch(params: ParamMatch) {
    try {
        const json = await fetchApi("fixtures", genParam(params));
        
        for (const res of json.response) {
            const _l = res.league;
            const season = await checkSeasonExist(_l.id + "-" + _l.season, _l);
            const _m = res.fixture;

            const match = await Match.find(_m.id) ?? new Match();
            match.seasonCode = season.code;
            match.date = new Date(_m.date);
            match.status = _m.status.short;
            match.score = JSON.stringify(res.score);

            if (match.id) await match.update();
            else {
                match.id = _m.id;
                await match.save();
            }

            for (const _t of res.teams) {
                const team = await checkTeamExist(_t.id, _t);

                const code = match.id + "-" + team.id;
                const head = await Head.find(code) ?? new Head();
                head.matchId = match.id;
                head.teamId = team.id;
                head.isHome = res.teams.home.id == team.id;

                if (head.code) await head.update();
                else {
                    head.code = code;
                    await head.save();
                }
            }
        }
    } catch(e) {
        console.error(e);
    }
}

export async function fetchLineup(params: ParamLineup) {
    try {
        const match = await Match.find(params.fixture);
        if (!match) return;
        const json = await fetchApi("fixtures/lineups", genParam(params));
        
        for (const res of json.response) {
            const head = await checkHeadExist(match.id + "-" + res.team.id, { ...res.team, isHome: json.response[0].team.id == res.team.id });
            const coach = await checkCoachExist(res.coach.id, { ...res.coach, teamId: head.teamId });
            head.coachId = coach.id;
            head.form = res.formation;
            await head.update();

            for (const _l of [...res.startXI, ...res.substitutes]) {
                const _p = _l.player;
                const player = await checkPlayerExist(_p.id, _p);

                const code = head.code + "-" + player.id;
                const lineup = await Lineup.find(code) ?? new Lineup();
                lineup.headCode = head.code;
                lineup.playerId = player.id;
                lineup.no = _p.number;
                lineup.pos = _p.pos;
                if (_p.grid) {
                    lineup.grid = _p.grid;
                    lineup.in = 0;
                }

                if (lineup.code) await lineup.update();
                else {
                    lineup.code = code;
                    await lineup.save();
                }
            }
        }
    } catch(e) {
        console.error(e);
    }
}

export async function fetchIncident(params: ParamIncident) {
    try {
        const match = await Match.find(params.fixture) as Match;
        if (!match) return;

        const head2head = await Head.where("matchId", match.id).get() as Head[];
        if (!head2head || head2head.length != 2) return;

        const lineupObj: {[key: string]: Lineup} = {};
        for (const h of head2head) {
            (await Lineup.where("headCode", h.code).get() as Lineup[]).map(l => lineupObj[l.code] = l);
        }

        const json = await fetchApi("fixtures/events", genParam(params));
        
        let i = 0;
        for (const res of json.response) {
            const _i = res;
            const code = match.id + "-" + (++i);
            const headCode = match.id + "-" + _i.team.id;

            const main = lineupObj[headCode + "-" + _i.player.id];
            if (!main) continue;

            const incident = await Incident.find(code) ?? new Incident();
            incident.matchId = match.id;
            incident.seq = i;
            incident.time = _i.time.elapsed;
            incident.extra = _i.time.extra;
            incident.mainCode = main.code;
            if (_i.assist.id) {
                const sub = lineupObj[headCode + "-" + _i.assist.id];
                incident.subCode = sub?.code;
            }
            incident.type = _i.type;
            incident.detail = _i.detail;

            if (incident.code) await incident.update();
            else {
                incident.code = code;
                await incident.save();
            }
        }
    } catch(e) {
        console.error(e);
    }
}

export async function fetchMatchStatistics(params: ParamMatchStats) {
    try {
        const match = await Match.find(params.fixture) as Match;
        if (!match) return;

        const head2head = await Head.where("matchId", match.id).get() as Head[];
        if (!head2head || head2head.length != 2) return;

        const lineupObj: {[key: string]: Lineup} = {};
        for (const h of head2head) {
            (await Lineup.where("headCode", h.code).get() as Lineup[]).map(l => lineupObj[l.code] = l);
        }

        const json = await fetchApi("fixtures/players", genParam(params));
        
        for (const res of json.response) {
            const _t = res.team;
            const headCode = match.id + "-" + _t.id;

            for (const _p of res.players) {
                const lineup = lineupObj[headCode + "-" + _p.player.id];
                if (!lineup) continue;

                lineup.stats = JSON.stringify(_p.statistics[0]);
                await lineup.update();
            }
        }
    } catch(e) {
        console.error(e);
    }
}

async function checkLeagueExist(id: string, data: Record<string, any>): Promise<League> {
    let league = await League.find(id);
    if (!league) {
        league = new League();
        league.id = id;
        league.name = data.name;
        if (data.logo) league.logo = data.logo;
        await league.save();
    }
    return league as League;
}

async function checkSeasonExist(id: string, data: Record<string, any>): Promise<Season> {
    let season = await Season.find(id);
    if (!season) {
        const code = id.split("-");
        const league = await checkLeagueExist(code[0], data);
        season = new Season();
        season.code = id;
        season.league_id = league.id;
        season.year = Number(code[1]);
        season.isCur = season.year == trySeason();
        await season.save();
    }
    return season as Season;
}

async function checkPlayerExist(id: string, data: Record<string, any>): Promise<Player> {
    let player = await Player.find(id);
    if (!player) {
        player = new Player();
        player.id = data.id;
        player.name = data.name;
        if (data.photo) player.photo = data.photo;
        if (data.teamId) player.teamId = data.teamId;
        await player.save();
    }
    return player as Player;
}

async function checkCoachExist(id: string, data: Record<string, any>): Promise<Coach> {
    let coach = await Coach.find(id);
    if (!coach) {
        coach = new Coach();
        coach.id = data.id;
        coach.name = data.name;
        if (data.photo) coach.photo = data.photo;
        if (data.teamId) coach.teamId = data.teamId;
        await coach.save();
    }
    return coach as Coach;
}

async function checkTeamExist(id: string, data: Record<string, any>): Promise<Team> {
    let team = await Team.find(id);
    if (!team) {
        team = new Team();
        team.id = data.id;
        team.name = data.name;
        team.tla = genTLA(data.name);
        if (data.logo) team.logo = data.logo;
        await team.save();
    }
    return team as Team;
}

async function checkHeadExist(id: string, data: Record<string, any>): Promise<Head> {
    let head = await Head.find(id);
    if (!head) {
        const code = id.split("-");
        const team = await checkTeamExist(code[1], data);
        head = new Head();
        head.code = id;
        head.matchId = Number(code[0]);
        head.teamId = team.id;
        head.isHome = data.isHome;
        await head.save();
    }
    return head as Head;
}